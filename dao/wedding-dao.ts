import { Wedding } from "../src/entities";

export interface WeddingDAO{

    //CREATE
    createWedding(wedding:Wedding):Promise<Wedding>;
    //createWedding

//READ
    gettAllWeddings():Promise<Wedding[]>;
    getWeddingById(weddingId:number):Promise<Wedding>;
    //getAllWeddings
    //getWeddingById

//UPDATE
    updateWedding(wedding:Wedding):Promise<Wedding>;
    //updateWedding

//DELETE
    deleteWeddingById(weddingId:number):Promise<boolean>;
    //deleteWeddingById
    //deleteWedding
}
