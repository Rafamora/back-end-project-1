import { Expense, Wedding } from "../src/entities";
import { ExpenseDAO } from "./expense-dao";
import { client } from "../src/connection";
import { MissingResourceError } from "../src/errors";


export class ExpenseDaoPostgres implements ExpenseDAO{
    
    async getExpensesByWeddingId(weddingId: number): Promise<Expense[]> {
        const sql:string = 'select * from expense where w_id=$1';
        const value = [weddingId];
        const result = await client.query(sql,value);
        const expenses:Expense[] = []
            for(const row of result.rows){
        const expense:Expense = new Expense(
            row.w_id,
            row.expense_id,
            row.expense_reason,
            row.expense_amount);
    expenses.push(expense);
}
return expenses;
    }
 
   async createExpense(expense: Expense): Promise<Expense> {
        const sql:string = "insert into expense(expense_reason,expense_amount,w_id) values ($1,$2,$3) returning expense_id";
        const values = [expense.expenseReason,expense.expenseAmount, expense.weddingId];
        const result = await client.query(sql,values);
        expense.expenseId= result.rows[0].expense_id;
        return expense;
    }

    async getAllExpenses(): Promise<Expense[]> {
        const sql:string = 'select * from expense';
        const result = await client.query(sql);
        const expenses:Expense[] = [];
        for(const row of result.rows){
            const expense:Expense = new Expense(
                row.w_id,
                row.expense_id,
                row.expense_reason,
                row.expense_amount);
            expenses.push(expense);
        }
        return expenses;
    }

    async getExpenseById(expenseId: number): Promise<Expense> {
        const sql:string = 'select * from expense where expense_id = $1';
        const values = [expenseId]
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expenseId} does not exist`);
        }
        const row = result.rows[0];
        const expense:Expense = new Expense(
            row.w_id,
            row.expense_id,
            row.expense_reason,
            row.expense_amount);
        return expense;
    }

    async updateExpense(expense: Expense): Promise<Expense> {
        const sql:string = 'update expense set expense_Id=$1, expense_Reason=$2, expense_Amount=$3 where expense_id=$4';
        const values = [expense.expenseId,expense.expenseReason,expense.expenseAmount, expense.expenseId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expense.expenseId} does not exist`);
        }
        return expense;
        }


    async deleteExpenseById(expenseId:number): Promise<boolean> {
        const sql:string = 'delete from expense where expense_id=$1';
        const values = [expenseId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The book with ID ${expenseId} does not exist`);
        }
        return true;
    }
   
}