import { Wedding } from "../src/entities";
import { WeddingDAO } from "./wedding-dao";
import { client } from "../src/connection";
import { MissingResourceError } from "../src/errors";


export class WeddingDaoPostgres implements WeddingDAO{
    
    async deleteWeddingById(weddingId: number):Promise<boolean>{
        const sql:string = 'delete from wedding where wedding_id=$1';
        const values = [weddingId];
        const result = await client.query(sql,values);
        return true;
    }
    

    async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = "insert into wedding(wedding_Name,wedding_Date,wedding_Location,wedding_Budget) values ($1,$2,$3,$4) returning wedding_Id";
        const values = [wedding.weddingName,wedding.weddingDate,wedding.weddingLocation,wedding.weddingBudget];
        const result = await client.query(sql,values);
        wedding.weddingId = result.rows[0].wedding_id;
        return wedding;
    }

    async gettAllWeddings(): Promise<Wedding[]> {
        const sql:string = 'select * from wedding';
        const result = await client.query(sql);
        const weddings:Wedding[] = [];
        for(const row of result.rows){
            const wedding:Wedding = new Wedding(
                row.wedding_id,
                row.wedding_name,
                row.wedding_date,
                row.wedding_location,
                row.wedding_budget);
            weddings.push(wedding);
        }
        return weddings;
}

    async getWeddingById(weddingId: number): Promise<Wedding> {
        const sql:string = 'select * from wedding where wedding_id = $1';
        const values = [weddingId]
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${weddingId} does not exist`);
        }
        const row = result.rows[0];
        const wedding:Wedding = new Wedding(
            row.wedding_id,
            row.wedding_name,
            row.wedding_date,
            row.wedding_location,
            row.wedding_budget);
        return wedding;
    }

    async updateWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = 'update wedding set wedding_Id=$1, wedding_Name=$2, wedding_Date=$3, wedding_Location=$4, wedding_Budget=$5 where wedding_id=$6';
        const values = [wedding.weddingId,wedding.weddingName,wedding.weddingDate,wedding.weddingLocation,wedding.weddingBudget,wedding.weddingId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${wedding.weddingId} does not exist`);
        }
        return wedding;
    }
}