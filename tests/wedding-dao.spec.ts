import { WeddingDAO } from '../dao/wedding-dao';
import { WeddingDaoPostgres } from '../dao/wedding-dao-postgres';
import { client } from '../src/connection';
import { Wedding } from '../src/entities';

const weddingDAO:WeddingDAO = new WeddingDaoPostgres();

const testWedding:Wedding = new Wedding(0, "Mora", "10-30-21", "Chicago", 50000);

test ("Create a wedding", async () =>{
    const result:Wedding = await weddingDAO.createWedding(testWedding);
    expect(result.weddingId).not.toBe(0);
})

test ("Get wedding by Id", async () =>{
    let wedding:Wedding = new Wedding(0, "Wille", "11-30-21", "Cancun", 45000);
    wedding = await weddingDAO.createWedding(wedding);
    console.log(wedding.weddingId)
    let retrievedWedding:Wedding = await weddingDAO.getWeddingById(wedding.weddingId);
    console.log(wedding.weddingId)
    expect(retrievedWedding.weddingId).toBe(wedding.weddingId);
})

test("Get all weddings", async ()=>{
    let wedding1:Wedding = new Wedding(1, "Dundee", "12-24-21", "Milwaukee", 50000);
    let wedding2:Wedding = new Wedding(0, "Johnson","6-5-22","Scottsdale", 37000);
    let wedding3:Wedding = new Wedding(3, "Bowes", "3-24-21", "Boston", 42000);
    await weddingDAO.createWedding(wedding1);
    await weddingDAO.createWedding(wedding2);
    await weddingDAO.createWedding(wedding3);

    const weddings:Wedding[] = await weddingDAO.gettAllWeddings();

    expect(weddings.length).toBeGreaterThanOrEqual(3);
});

test("update wedding", async ()=>{
    let wedding:Wedding = new Wedding(10, "Sanchez", "1-15-2022", "Manhattan", 90000);
    wedding = await weddingDAO.createWedding(wedding);
});

test("delete wedding by id", async ()=>{
    let wedding:Wedding = new Wedding(12, "Williams", "5-22-22", "Destin", 30000);
    wedding = await weddingDAO.createWedding(wedding);
});


afterAll(async()=>{
    client.end()
})


