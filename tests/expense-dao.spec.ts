import { ExpenseDAO } from "../dao/expense-dao";
import { ExpenseDaoPostgres } from "../dao/expense-dao-postgres";
import { client } from "../src/connection";
import { Expense } from '../src/entities';

const expenseDAO:ExpenseDAO = new ExpenseDaoPostgres();

const testExpense:Expense = new Expense(1, 0, "Deposit", 2000);

test ("Create an expense", async () =>{
    const result:Expense = await expenseDAO.createExpense(testExpense);
    expect(result.expenseId).not.toBe(0);
})

test ("Get expense by Id", async () =>{    
    let expense:Expense = new Expense(34, 12, "Down Payment", 6000);
    expense = await expenseDAO.createExpense(expense);

    let retreivedExpense:Expense = await expenseDAO.getExpenseById(expense.expenseId);
    expect(retreivedExpense.expenseId).toBe(retreivedExpense.expenseId);
})

test("Get all expenses", async ()=>{
    let expense1:Expense = new Expense(3, 9, "Deposit", 3000);
    let expense2:Expense = new Expense(1, 4, "Deposit", 5000);
    let expense3:Expense = new Expense(2, 3, "Deposit", 1000);
    await expenseDAO.createExpense(expense1);
    await expenseDAO.createExpense(expense2);
    await expenseDAO.createExpense(expense3);

    const expenses:Expense[] = await expenseDAO.getAllExpenses();

    expect(expenses.length).toBeGreaterThanOrEqual(1);
});

test("update expense", async ()=>{
    let expense:Expense = new Expense(10,10,"Food Deposit",4500);
    expense = await expenseDAO.createExpense(expense);
});

test("delete expense by id", async ()=>{
    let expense:Expense = new Expense(15,13,"DJ",1200);
    expense = await expenseDAO.createExpense(expense);
});


afterAll(async()=>{
    client.end()
})
