
export class Wedding{
    constructor(
        public weddingId:number,
        public weddingName:string,
        public weddingDate:string,
        public weddingLocation:string,
        public weddingBudget:number,
    ){}
}

export class Expense{
    constructor(
        public weddingId:number,
        public expenseId:number,
        public expenseReason:string,
        public expenseAmount:number,
    ){}
}