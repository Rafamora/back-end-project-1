import express from 'express';
import ExpenseService from '../services/expense-service';
import WeddingService from '../services/wedding-service';
import { WeddingServiceImpl } from '../services/wedding-service-impl';
import { ExpenseServiceImpl } from '../services/expense-service-impl';
import { Wedding } from './entities';
import { Expense } from './entities';
import { number } from 'yargs';
import cors from 'cors'
import { MissingResourceError } from './errors';

const app = express();
app.use(express.json());
app.use(cors());

const weddingService:WeddingService = new WeddingServiceImpl();
const expenseService:ExpenseService = new ExpenseServiceImpl();

app.get("/weddings", async (req, res) => {
    const weddings:Wedding[] = await weddingService.retreiveAllWeddings();
    res.send(weddings);
});

app.get("/weddings/:id", async (req, res) => {
try{
    const weddingId = Number(req.params.id);
    const wedding:Wedding = await weddingService.retreiveWeddingById(weddingId);
    res.send(wedding);
} catch (error){
    if(error instanceof MissingResourceError){
        res.status(404);
        res.send(error);
    }
}
});

app.get("/expenses/:id", async (req, res) => {
    try{
        const expenseId = Number(req.params.id);
        const expense:boolean = await expenseService.removeExpenseById(expenseId);
        res.send(expense);
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
    });

app.post("/weddings", async (req, res) => {
    let wedding:Wedding = req.body;
    wedding = await weddingService.registerWedding(wedding);
    res.send(wedding);
});

app.get("/expenses", async (req, res) => {
    const expenses:Expense[] = await expenseService.retreiveAllExpenses();
    res.send(expenses);
});

app.post("/expenses", async (req, res) => {
    let expense:Expense = req.body;
    expense = await expenseService.registerExpense(expense);
    res.send(expense);
});

app.delete("/weddings/:id", async (req, res)=>{
    try{
    const weddingId = Number(req.params.id);
    let result:Boolean = await weddingService.removeWeddingById(weddingId)
    const wedding = await weddingService.removeWeddingById(weddingId);
    res.send(wedding)
    }catch(error){
        if(error){
            res.status(404);
            res.send(error);
        }

    }
});

app.delete("/expenses/:id", async (req, res)=>{
    const expenseId = Number(req.params.id);
    const expense = await expenseService.removeExpenseById(expenseId);
    res.send(expense)

});

app.put("/weddings/:id", async (req, res)=>{
    const weddingId:number = Number(req.params.id);
    const wedding:Wedding = req.body
    const updateWedding = await weddingService.updateWedding(wedding);
    res.send(wedding)
});

app.put("/expenses/:id", async (req, res)=>{
    const expenseId:number = Number(req.params.id);
    const expense:Expense = req.body
    const updateExpense = await expenseService.updateExpense(expense);
    res.send(expense)
});

app.get("/weddings/:id/expenses", async (req, res)=>{
    try {
        const weddingId = Number(req.params.id);
        const expenses:Expense[] = await expenseService.retreiveExpensebyWeddingId(weddingId);
    res.send(expenses);
    }
    catch (error) {
    if(error instanceof MissingResourceError){
    res.status(404);
    res.send(error);
    }}
    });


app.listen(3002,()=>{console.log("Application Started")})