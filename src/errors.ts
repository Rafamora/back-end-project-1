
export class MissingResourceError{

    message:string;
    description:string = "Unfortunately, this resource could not be found. Please try again.";

    constructor(message:string){
        this.message = message;
    }
}
