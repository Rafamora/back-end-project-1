import { Expense } from '../src/entities';

export default interface ExpenseService{

    registerExpense(expense:Expense):Promise<Expense>;

    retreiveAllExpenses():Promise<Expense[]>;

    retreiveExpenseById(expenseId:number):Promise<Expense>;

    searchExpense(expenseId:number):Promise<Expense>;

    removeExpenseById(expenseId:number):Promise<boolean>;

    updateExpense(expense:Expense):Promise<Expense>;

    retreiveExpensebyWeddingId(weddingId:number):Promise<Expense[]>;
    

}

