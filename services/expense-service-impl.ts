import { ExpenseDAO } from "../dao/expense-dao";
import { ExpenseDaoPostgres } from "../dao/expense-dao-postgres";
import { Expense } from "../src/entities";
import ExpenseService from "./expense-service"

export class ExpenseServiceImpl implements ExpenseService{
    retreiveExpensebyWeddingId(wedding: number): Promise<Expense[]> {
        return this.expenseDAO.getExpensesByWeddingId(wedding);
    }
    
    expenseDAO:ExpenseDAO = new ExpenseDaoPostgres();

    updateExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.updateExpense(expense);
    }

    
    async removeExpenseById(expenseId:number): Promise<boolean> {
        const expense:Expense = await this.expenseDAO.getExpenseById(expenseId);
        return this.expenseDAO.deleteExpenseById(expenseId);
        
    }

    registerExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.createExpense(expense);
    }

    retreiveAllExpenses(): Promise<Expense[]> {
        return this.expenseDAO.getAllExpenses();
    }

    retreiveExpenseById(expenseId: number): Promise<Expense> {
        return this.expenseDAO.getExpenseById(expenseId);
    }

    searchExpense(expenseId: number): Promise<Expense> {
        return this.expenseDAO.getExpenseById(1)
    }
    
}