import { WeddingDAO } from "../dao/wedding-dao";
import { Wedding } from "../src/entities";
import WeddingService from "./wedding-service";
import { isThisTypeNode } from "typescript";
import { WeddingDaoPostgres } from "../dao/wedding-dao-postgres";

export class WeddingServiceImpl implements WeddingService{
    
    weddingDAO:WeddingDAO = new WeddingDaoPostgres();

    
    updateWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.updateWedding(wedding)
    }

    registerWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.createWedding(wedding);
    }

    retreiveAllWeddings(): Promise<Wedding[]> {
        return this.weddingDAO.gettAllWeddings();
    }

    retreiveWeddingById(weddingId: number): Promise<Wedding> {
        return this.weddingDAO.getWeddingById(weddingId);
    }

    searchByName(weddingName: string): Promise<Wedding> {
        return this.weddingDAO.getWeddingById(1)
    }
    
    removeWeddingById(weddingId: number): Promise<boolean> {
        return this.weddingDAO.deleteWeddingById(weddingId);
    } 
}